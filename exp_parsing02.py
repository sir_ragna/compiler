#!/usr/bin/python3

import cradle as c

#cradle.init("x = 2*y + 3/(4*z)")
#cradle.expression()

print("# Execute 2")
c.init('2')
c.term()

print("# Execute 2+4")
c.init('2+4')
c.expression()

print("# Execute 4-3")
c.init('4-3')
c.expression()

print("# Execute 2*3")
c.init('2*3')
c.expression()


print("# Execute 1+3-1*6")
c.init('1+3-1*6')
c.expression()

print("# Execute  2*(3+4)")
c.init("2*(3+4)")
c.expression()

print("# Execute (1+2)/((3+4)+(5-6))")
c.init("(1+2)/((3+4)+(5-6))")
c.expression()

print("# Execute -4")
c.init("-4")
c.expression()
