#!/usr/bin/python3

offset  = -12  # stack offset
size    = 4    # size in bytes
current = offset
variables = {}

def get_index(name):
    global current
    global size
    global offset
    if name in variables:
        return variables[name]
    else:
        current -= size
        variables[name] = current
        return current

def get_location(name):
    return '{index}(%rbp)'.format(index=get_index(name))

if __name__ == '__main__':
    print(get_location('x'))
    print(get_location('q'))
    print(get_location('x'))
    print(get_location('p'))
    print(get_location('z'))
    print(get_location('x'))