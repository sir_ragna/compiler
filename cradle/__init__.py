#!/usr/bin/python3

from cradle.variables import get_location

text  = ''
look  = '' # Lookahead Character
alpha = [chr(i) for i in range(ord('A'), (ord('Z') + 1))]
digit = [str(i) for i in range(0, 10)]

def error(msg):
    print("Error :: %s" % msg)

def abort(msg):
    error(msg)
    exit(1)

def expected(s):
    abort("%s :: Expected" % s)

def match(x):
    global look
    if look == x:
        return get_char()
    else:
        expected("\"%s\"" % x)

def is_alpha(c):
    return c.upper() in alpha

def is_digit(c):
    return c in digit

def get_name():
    global look
    if not is_alpha(look):
        expected('Name')
    ret_char = look.upper()
    get_char()
    return char

def get_num():
    global look
    if not is_digit(look):
        expected("Integer")
    ret_char = look
    get_char()
    return ret_char
    

def emit(s):
    print("\t%s" % s, end="")

def emit_ln(s):
    emit("%s\n" % s)

def get_char():
    global text
    global look
    look = text[0:1]
    text = text[1:]

def subtract():
    match('-')
    term()
    emit_ln('popl %edx')
    emit_ln('subl %eax, %edx # %eax - %edx' )
    emit_ln('movl %edx, %eax # store result in %edx')
    emit_ln('\t# ^ otherwise we could:')
    emit_ln('\t# %subl %edx, %eax')
    emit_ln('\t# negate %eax')
    
def add():
    match('+')
    term()
    emit_ln('popl %edx')
    emit_ln('addl %edx %eax  # %eax += %edx')
    
def multiply():
    match('*')
    factor()
    emit_ln('popl %edx')
    emit_ln('imull %edx %eax  # %eax *= %edx')
    
def divide():
    match('/')
    factor()
    emit_ln('popl %ebx')
    emit_ln('cltd             # clear %edx')
    emit_ln('idivl %ebx %eax  # %eax = %eax / %ebx')
    emit_ln('                 # remainder is stored in %edx')
    emit_ln('xorl %edx %edx   # clear remainder/%edx')
    
def expression():
    if is_operator(look):
        # Checks for leading operators (-1) + (+3)
        emit_ln('xorl %eax %eax # zero out %edx')
        emit_ln('               # leading operator we translate this to')
        emit_ln('               # 0 - x | 0 + x')
    else:
        term()
        
    while is_operator(look):
        # push on stack
        emit_ln('pushl %eax')
        if '+' == look:
            add()
        elif '-' == look:
            subtract()
        else: # useless check?
            expected('add/sub operator')

def is_operator(char):
    return char in ['+', '-']

def factor():
    if look == '(':
        match('(')
        expression()
        match(')')
    elif is_alpha(look):
        emit_ln('movel {0}, %eax'.format(get_location(get_name)))
    else:
        emit_ln('movel ${0}, %eax'.format(get_num()))
            
def term():
    factor()
    while look in ['*', '/']:
        emit_ln('pushl %eax')
        if '*' == look:
            multiply()
        elif '/' == look:
            divide()
        else:
            expected('mul/div operator')

    
def init(txt):
    global text
    text = txt
    get_char()