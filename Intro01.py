#!/usr/bin/python3

import cradle
import codecs

# http://www.compilers.iecc.com/crenshaw/tutor1.txt

fh = codecs.open('input.txt', 'r', 'utf-8', 'strict')

cradle.init(fh.read())

while cradle.text != "":
    cradle.emit_ln(cradle.look)
    cradle.get_char()

cradle.emit_ln(cradle.look)

