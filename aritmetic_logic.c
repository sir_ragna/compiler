#pragma optimize level=0
#include <stdio.h>

int main(void) {
  int result = arAndLogic(1, 2, 3);
  printf("%d", result);
}

int arAndLogic(int x, int y, int z) {
  int t1 = x + y;
  int t2 = z * 48;
  int t3 = t1 & 0xFFFF;
  int t4 = t2 * t3;
  return t4;
}
