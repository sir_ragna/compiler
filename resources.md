# Resources #

## M68000 ##

- Motorala 68000 instructions
  [http://68k.hax.com/]()
- Arch[https://en.wikipedia.org/wiki/Motorola_68000]()

## Intel x86 ##

- Instructions [https://en.wikipedia.org/wiki/X86_instruction_listings]()
- Syntax [https://en.wikibooks.org/wiki/X86_Assembly/GAS_Syntax]()
- Wikibook [https://en.wikibooks.org/wiki/X86_Assembly]()

## Let's build a compiler ##

- [http://www.compilers.iecc.com/crenshaw/]()