
# Let's Build a Compiler

My attempt at translating http://compilers.iecc.com/crenshaw/ to Python.

I am currently stuck at getting the x86 code executable and into some kind of debuggable format.

The compiler itself is the module 'Cradle'. `Intro01.py` and `exp_parsing02.py` call this to produce output.

Project currently on hold since my Assembler knowledge is too limited. Might resume when I find a tutor or when my assembler knowledge has expanded enough.


--Robbe

